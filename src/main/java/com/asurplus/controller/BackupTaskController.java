package com.asurplus.controller;

import com.asurplus.common.utils.RES;
import com.asurplus.common.vo.BackupTaskUpdateReqVO;
import com.asurplus.common.vo.BackupTaskVO;
import com.asurplus.common.vo.LayuiTablePojo;
import com.asurplus.entity.BackupTask;
import com.asurplus.service.BackupTaskService;
import com.asurplus.service.NodeInfoService;
import com.asurplus.service.ZoneInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @since 2022-08-05
 */
@Controller
@RequestMapping("backup-task")
public class BackupTaskController {

    @Autowired
    private BackupTaskService backupTaskService;
    @Autowired
    private ZoneInfoService zoneInfoService;
    @Autowired
    private NodeInfoService nodeInfoService;

    /**
     * 任务管理首页
     */
    @GetMapping("")
    public String list(Model model) {
        model.addAttribute("zone", zoneInfoService.listSelect());
        model.addAttribute("node", nodeInfoService.listSelect(null));
        return "backup-task/index";
    }

    /**
     * 任务管理修改页
     */
    @GetMapping("backup-task-update/{id}")
    public String update(@PathVariable("id") Integer id, Model model) {
        model.addAttribute("data", backupTaskService.getById(id));
        return "backup-task/update";
    }

    /**
     * 备份任务分页查询
     */
    @PostMapping("list")
    @ResponseBody
    public LayuiTablePojo list(Integer page, Integer limit, BackupTaskVO backupTask) {
        return backupTaskService.list(page, limit, backupTask);
    }

    /**
     * 备份任务修改
     */
    @PostMapping("update")
    @ResponseBody
    public RES update(@RequestBody BackupTaskUpdateReqVO reqVO) {
        return backupTaskService.update(reqVO);
    }

    /**
     * 备份任务执行一次
     */
    @GetMapping("run/{id}")
    @ResponseBody
    public RES run(@PathVariable("id") Integer id) {
        return backupTaskService.run(id);
    }

    /**
     * 备份任务启动/停止
     */
    @GetMapping("updateStatus/{id}/{status}")
    @ResponseBody
    public RES updateStatus(@PathVariable("id") Integer id, @PathVariable("status") Boolean status) {
        BackupTask backupTask = new BackupTask();
        backupTask.setId(id);
        backupTask.setStatus(status);
        return backupTaskService.updateStatus(backupTask);
    }

    /**
     * 备份任务删除
     */
    @GetMapping("delete/{id}")
    @ResponseBody
    public RES delete(@PathVariable("id") Integer id) {
        return backupTaskService.delete(id);
    }
}
