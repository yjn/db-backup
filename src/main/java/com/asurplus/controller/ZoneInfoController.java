package com.asurplus.controller;

import com.asurplus.entity.ZoneInfo;
import com.asurplus.common.vo.LayuiTablePojo;
import com.asurplus.service.ZoneInfoService;
import com.asurplus.common.utils.RES;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import org.springframework.stereotype.Controller;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @since 2022-08-05
 */
@Controller
@RequestMapping("zone-info")
public class ZoneInfoController {

    @Autowired
    private ZoneInfoService zoneInfoService;

    /**
     * 分区管理首页
     */
    @GetMapping("")
    public String list() {
        return "zone-info/index";
    }

    /**
     * 分区管理新增页
     */
    @GetMapping("zone-info-add")
    public String add() {
        return "zone-info/add";
    }

    /**
     * 分区管理修改页
     */
    @GetMapping("zone-info-update/{id}")
    public String update(@PathVariable("id") Integer id, Model model) {
        model.addAttribute("data", zoneInfoService.getById(id));
        return "zone-info/update";
    }

    /**
     * 分区信息分页查询
     */
    @PostMapping("list")
    @ResponseBody
    public LayuiTablePojo list(Integer page, Integer limit, ZoneInfo zoneInfo) {
        return zoneInfoService.list(page, limit, zoneInfo);
    }

    /**
     * 分区信息新增
     */
    @PostMapping("add")
    @ResponseBody
    public RES add(@RequestBody ZoneInfo zoneInfo) {
        return zoneInfoService.add(zoneInfo);
    }

    /**
     * 分区信息修改
     */
    @PostMapping("update")
    @ResponseBody
    public RES update(@RequestBody ZoneInfo zoneInfo) {
        return zoneInfoService.update(zoneInfo);
    }

    /**
     * 分区信息删除
     */
    @GetMapping("delete/{id}")
    @ResponseBody
    public RES delete(@PathVariable("id") Integer id) {
        return zoneInfoService.delete(id);
    }
}
