package com.asurplus.controller;

import com.asurplus.service.NodeInfoService;
import com.asurplus.common.vo.LayuiTablePojo;
import com.asurplus.service.BackupLogService;
import com.asurplus.service.ZoneInfoService;
import com.asurplus.common.utils.RES;
import com.asurplus.common.vo.BackupLogVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @since 2022-08-05
 */
@Controller
@RequestMapping("backup-log")
public class BackupLogController {

    @Autowired
    private BackupLogService backupLogService;
    @Autowired
    private ZoneInfoService zoneInfoService;
    @Autowired
    private NodeInfoService nodeInfoService;

    /**
     * 备份记录首页
     */
    @GetMapping("")
    public String list(Model model) {
        model.addAttribute("zone", zoneInfoService.listSelect());
        model.addAttribute("node", nodeInfoService.listSelect(null));
        return "backup-log/index";
    }

    /**
     * 备份记录详情页
     */
    @GetMapping("backup-log-detail/{id}")
    public String detail(@PathVariable("id") Integer id, Model model) {
        model.addAttribute("zone", zoneInfoService.listSelect());
        model.addAttribute("data", backupLogService.getBackUpLogVO(id));
        return "backup-log/detail";
    }

    /**
     * 备份记录分页查询
     */
    @PostMapping("list")
    @ResponseBody
    public LayuiTablePojo list(Integer page, Integer limit, BackupLogVO backUpLogVO) {
        return backupLogService.list(page, limit, backUpLogVO);
    }

    /**
     * 删除备份记录
     */
    @GetMapping("delete/{id}")
    @ResponseBody
    public RES delete(@PathVariable("id") Integer id) {
        return backupLogService.delete(id);
    }

    /**
     * 清空备份记录
     */
    @GetMapping("deleteAll")
    @ResponseBody
    public RES deleteAll() {
        return backupLogService.deleteAll();
    }
}
