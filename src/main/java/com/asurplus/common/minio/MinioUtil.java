package com.asurplus.common.minio;

import com.asurplus.common.utils.FileUtil;
import io.minio.MinioClient;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.UUID;

/**
 * minio工具类
 *
 * @author asurplus
 */
@Slf4j
@Component
public class MinioUtil {

    @Autowired
    private MinioClient client;

    /**
     * 创建bucket
     *
     * @param bucketName bucket名称
     */
    @SneakyThrows
    public void createBucket(String bucketName) {
        if (!client.bucketExists(bucketName)) {
            client.makeBucket(bucketName);
        }
    }

    /**
     * 上传文件
     *
     * @param file
     * @param dir
     * @return
     */
    public String uploadFile(File file, String dir) {
        if (null == file || 0 == file.length()) {
            return null;
        }
        return uploadFile(FileUtil.file2MultipartFile(file), dir);
    }

    /**
     * 上传文件
     *
     * @param file 文件
     * @param dir
     * @return
     */
    public String uploadFile(MultipartFile file, String dir) {
        // 判断上传文件是否为空
        if (null == file || file.isEmpty()) {
            return null;
        }
        try {
            // 判断存储桶是否存在
            createBucket(MinioConstant.Bucket.DEFAULT_BUCKET);
            // 原始文件名
            String originalFilename = file.getOriginalFilename();
            // 文件后缀 例如：png
            String fileSuffix = originalFilename.substring(originalFilename.lastIndexOf(".") + 1);
            // uuid 生成文件名
            String uuid = String.valueOf(UUID.randomUUID());
            // 新的文件名
            if (StringUtils.isBlank(dir)) {
                dir = MinioConstant.Dir.SYS_DEFAULT;
            }
            String fileName = dir + "/" + getYyyyMMdd() + "/" + uuid + "." + fileSuffix;
            // 开始上传
            client.putObject(MinioConstant.Bucket.DEFAULT_BUCKET, fileName, file.getInputStream(), file.getContentType());
            return client.getObjectUrl(MinioConstant.Bucket.DEFAULT_BUCKET, fileName);
        } catch (Exception e) {
            log.error("上传文件失败：{}", e.getMessage());
        }
        return null;
    }

    public static String getYyyyMMdd() {
        DateFormat df = new SimpleDateFormat("yyyyMMdd");
        return df.format(new Date());
    }
}
