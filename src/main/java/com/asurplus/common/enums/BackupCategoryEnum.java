package com.asurplus.common.enums;

import lombok.AllArgsConstructor;

/**
 * 备份类型枚举类
 *
 * @author asurplus
 */
@AllArgsConstructor
public enum BackupCategoryEnum {

    /**
     * 手动备份
     */
    MANUAL("0", "手动备份"),

    /**
     * 自动备份
     */
    AUTO("1", "自动备份");

    private final String code;

    /**
     * 返回消息
     */
    private final String msg;

    public String getCode() {
        return code;
    }

    public String getMsg() {
        return msg;
    }
}
