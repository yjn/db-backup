package com.asurplus.common.utils;

import io.swagger.annotations.ApiModelProperty;

import java.util.LinkedHashMap;

/**
 * 接口统一返回数据
 *
 * @author asurplus
 */
public class RES extends LinkedHashMap<String, Object> {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "状态码")
    public static final String CODE_TAG = "code";

    @ApiModelProperty(value = "提示信息")
    public static final String MSG_TAG = "msg";

    @ApiModelProperty(value = "返回数据")
    public static final String DATA_TAG = "data";

    /**
     * 无参构造方法
     */
    public RES() {

    }

    public RES(Integer code) {
        super.put(CODE_TAG, code);
    }

    public RES(Integer code, String msg) {
        super.put(CODE_TAG, code);
        super.put(MSG_TAG, msg);
    }

    public RES(Integer code, String msg, Object data) {
        super.put(CODE_TAG, code);
        super.put(MSG_TAG, msg);
        if (null != data) {
            super.put(DATA_TAG, data);
        }
    }

    /**
     * 请求成功返回
     *
     * @return
     */
    public static RES ok() {
        return new RES(200, "操作成功", null);
    }

    public static RES ok(String msg) {
        return new RES(200, msg, null);
    }

    public static RES ok(Object data) {
        return new RES(200, "操作成功", data);
    }

    public static RES ok(Integer code, String msg) {
        return new RES(code, msg, null);
    }

    public static RES ok(String msg, Object data) {
        return new RES(200, msg, data);
    }

    public static RES ok(Integer code, String msg, Object data) {
        return new RES(code, msg, data);
    }

    /**
     * 请求失败返回
     */

    public static RES no(Integer code, String msg) {
        return new RES(code, msg, null);
    }

    public static RES no(String msg) {
        return new RES(500, msg, null);
    }

    public static RES no(Integer code, String msg, Object data) {
        return new RES(code, msg, data);
    }

    /**
     * 获取code
     */
    public Integer getCode() {
        return Integer.parseInt(String.valueOf(this.get(CODE_TAG)));
    }

    /**
     * 获取msg
     */
    public String getMsg() {
        return String.valueOf(this.get(MSG_TAG));
    }

    /**
     * 获取data
     */
    public Object getData() {
        return this.get(DATA_TAG);
    }
}
