package com.asurplus.common.vo;

import com.asurplus.entity.ZoneInfo;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class ZoneInfoVO extends ZoneInfo {

    @ApiModelProperty("实例数")
    private Integer nodes;
}
