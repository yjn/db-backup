package com.asurplus.common.vo;

import lombok.Data;

/**
 * 数据库连接参数
 */
@Data
public class NodeProperties {

    /**
     * 驱动名
     */
    private String className;

    /**
     * 主机
     */
    private String host;

    /**
     * 端口
     */
    private String port;

    /**
     * 数据库
     */
    private String database;

    /**
     * 用户名
     */
    private String username;

    /**
     * 密码
     */
    private String password;
}
