package com.asurplus.common.job;

import com.alibaba.fastjson.JSONObject;
import com.asurplus.common.config.quartz.QuartzManager;
import com.asurplus.common.enums.BackupCategoryEnum;
import com.asurplus.common.utils.MysqlTool;
import com.asurplus.common.vo.BackupJobVO;
import com.asurplus.entity.BackupLog;
import com.asurplus.service.BackupLogService;
import lombok.extern.slf4j.Slf4j;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * 备份定时任务job
 *
 * @author asurplus
 */
@Slf4j
public class BackupJob implements Job {

    @Autowired
    private MysqlTool mysqlTool;

    @Autowired
    private BackupLogService backupLogService;

    @Override
    public void execute(JobExecutionContext context) throws JobExecutionException {
        // 解析参数
        BackupJobVO jobVO = JSONObject.parseObject(context.getJobDetail().getJobDataMap().getString(QuartzManager.PARAM_KEY), BackupJobVO.class);
        // 保存备份记录
        BackupLog backupLog = backupLogService.saveBackupLog(jobVO, BackupCategoryEnum.AUTO);
        // 开始备份
        mysqlTool.saveBackUp(jobVO, backupLog);
    }

}
