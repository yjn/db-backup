package com.asurplus.entity;

import com.baomidou.mybatisplus.annotation.*;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 *
 * </p>
 *
 * @since 2022-08-05
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("backup_log")
@ApiModel(value = "BackupLog对象", description = "备份记录")
public class BackupLog extends Model<BackupLog> {

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty(value = "数据库id")
    @TableField("node_id")
    private Integer nodeId;

    @ApiModelProperty(value = "来源（手动备份|自动备份）")
    @TableField("category")
    private String category;

    @ApiModelProperty(value = "库名")
    @TableField("database_name")
    private String databaseName;

    @ApiModelProperty(value = "表名")
    @TableField("tables_name")
    private String tablesName;

    @ApiModelProperty(value = "备份方式")
    @TableField("data_type")
    private Integer dataType;

    @ApiModelProperty(value = "是否压缩")
    @TableField("is_compress")
    private Boolean isCompress;

    @ApiModelProperty(value = "是否上传")
    @TableField("is_upload")
    private Boolean isUpload;

    @ApiModelProperty(value = "是否删除")
    @TableField("is_delete")
    private Boolean isDelete;

    @ApiModelProperty(value = "文件路径")
    @TableField("file_path")
    private String filePath;

    @ApiModelProperty(value = "文件大小")
    @TableField("file_size")
    private Long fileSize;

    @ApiModelProperty(value = "备份状态（-1-失败0-备份中1-上传中200-成功）")
    @TableField("status")
    private Integer status;

    @ApiModelProperty(value = "备份信息")
    @TableField("msg")
    private String msg;

    @ApiModelProperty(value = "开始时间")
    @TableField("start_time")
    private Date startTime;

    @ApiModelProperty(value = "结束时间")
    @TableField("end_time")
    private Date endTime;

    @ApiModelProperty(value = "耗时(ms)")
    @TableField("spend_time")
    private Long spendTime;

    @ApiModelProperty(value = "删除状态（0--未删除1--已删除）")
    @TableField("del_flag")
    @TableLogic
    private Integer delFlag;

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
