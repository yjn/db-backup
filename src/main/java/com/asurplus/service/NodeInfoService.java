package com.asurplus.service;

import com.asurplus.entity.NodeInfo;
import com.asurplus.common.utils.RES;
import com.asurplus.common.vo.BackupReqVO;
import com.asurplus.common.vo.LayuiTablePojo;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @since 2022-08-04
 */
public interface NodeInfoService extends IService<NodeInfo> {

    LayuiTablePojo list(Integer page, Integer limit, NodeInfo nodeInfo);

    RES add(NodeInfo nodeInfo);

    RES update(NodeInfo nodeInfo);

    RES delete(Integer id);

    RES saveBackUp(BackupReqVO reqVO);

    List<NodeInfo> listSelect(Integer zoneId);
}
