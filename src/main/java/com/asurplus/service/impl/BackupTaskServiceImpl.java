package com.asurplus.service.impl;

import com.alibaba.fastjson.JSON;
import com.asurplus.common.config.quartz.QuartzManager;
import com.asurplus.common.utils.RES;
import com.asurplus.common.vo.BackupTaskUpdateReqVO;
import com.asurplus.common.vo.BackupTaskVO;
import com.asurplus.common.vo.LayuiTablePojo;
import com.asurplus.entity.BackupTask;
import com.asurplus.mapper.BackupTaskMapper;
import com.asurplus.service.BackupTaskService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.apache.commons.lang3.StringUtils;
import org.quartz.CronExpression;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class BackupTaskServiceImpl extends ServiceImpl<BackupTaskMapper, BackupTask> implements BackupTaskService {

    @Autowired
    private QuartzManager quartzManager;

    @Override
    public LayuiTablePojo list(Integer page, Integer limit, BackupTaskVO backupTask) {
        QueryWrapper<BackupTaskVO> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("a.del_flag", 0);
        if (null != backupTask.getZoneId()) {
            queryWrapper.eq("b.zone_id", backupTask.getZoneId());
        }
        if (null != backupTask.getNodeId()) {
            queryWrapper.eq("a.node_id", backupTask.getNodeId());
        }
        queryWrapper.orderByDesc("a.create_time");
        return LayuiTablePojo.ok(this.baseMapper.list(new Page<>(page, limit), queryWrapper));
    }

    @Override
    public RES update(BackupTaskUpdateReqVO reqVO) {
        if (StringUtils.isBlank(reqVO.getCron())) {
            return RES.no("请输入cron表达式");
        }
        if (!CronExpression.isValidExpression(reqVO.getCron())) {
            return RES.no("请输入正确的cron表达式");
        }
        BackupTask backupTask = this.baseMapper.selectById(reqVO.getId());
        backupTask.setCron(reqVO.getCron());
        backupTask.setStatus(reqVO.getStatus());
        backupTask.setRemark(reqVO.getRemark());
        // 任务参数
        backupTask.getParam().setIsCompress(reqVO.getIsCompress());
        backupTask.getParam().setIsUpload(reqVO.getIsUpload());
        backupTask.getParam().setIsDelete(reqVO.getIsDelete());
        this.baseMapper.updateById(backupTask);
        // 修改定时任务
        quartzManager.update(backupTask.getId(), backupTask.getCron(), JSON.toJSONString(backupTask.getParam()), backupTask.getStatus());
        return RES.ok();
    }

    @Override
    public RES delete(Integer id) {
        if (null == id) {
            return RES.no("请选择需要操作的数据");
        }
        BackupTask backupTask = this.baseMapper.selectById(id);
        if (null == backupTask) {
            return RES.no("数据错误，请刷新后重试");
        }
        if (backupTask.getStatus()) {
            return RES.no("请先停止任务，再删除");
        }
        // 删除
        this.baseMapper.deleteById(backupTask);
        // 删除定时任务
        quartzManager.delete(id);
        return RES.ok();
    }

    @Override
    public RES run(Integer id) {
        if (null == id) {
            return RES.no("请选择需要操作的数据");
        }
        BackupTask backupTask = this.baseMapper.selectById(id);
        if (null == backupTask) {
            return RES.no("数据错误，请刷新后重试");
        }
        quartzManager.run(id);
        return RES.ok();
    }

    @Override
    public RES updateStatus(BackupTask reqVO) {
        if (null == reqVO.getId()) {
            return RES.no("请选择需要操作的数据");
        }
        BackupTask backupTask = this.baseMapper.selectById(reqVO.getId());
        if (null == backupTask) {
            return RES.no("数据错误，请刷新后重试");
        }
        backupTask.setStatus(reqVO.getStatus());
        this.baseMapper.updateById(backupTask);
        // 启动
        if (backupTask.getStatus()) {
            quartzManager.start(backupTask.getId());
        }
        // 停止
        else {
            quartzManager.stop(backupTask.getId());
        }
        return RES.ok();
    }
}
